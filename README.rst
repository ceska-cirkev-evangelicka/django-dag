Django DAG
##########

Django-dag is a small reusable app which implements a Directed Acyclic Graph.


Installation
------------

Best choice is to install from sources (you need Git):

.. code:: bash

    pip install -e 'git+https://bitbucket.org/ceska-cirkev-evangelicka/django-dag.git#egg=django-dag'


Then just simply add to ``INSTALLED_APPS`` in your settings file:

.. code:: python

    INSTALLED_APPS = (
        ...
        'django_dag',
    )


Usage
-----

Django-dag uses abstract base classes, to use it you must create your own
concrete classes that inherit from Django-dag classes.

The ``dag_test`` app contains a simple example and a unit test to show
you its usage.

Example:

.. code:: python
   
    from django.db import models
    from django_dag.models import node_factory, edge_factory    

    class ConcreteNode(node_factory('ConcreteEdge')):
        """
        Test node, adds just one field
        """
        name = models.CharField(max_length = 32)

    class ConcreteEdge (edge_factory(ConcreteNode, concrete = False)):
        """
        Test edge, adds just one field
        """
        name = models.CharField(max_length = 32, blank = True, null = True)
